#ifndef IMAGEHELPER_H
#define IMAGEHELPER_H

#include <QIcon>
#include <QObject>
#include <QPainter>
#include <QSvgRenderer>
#include <QWidget>

class ImageHelper : public QObject
{
    Q_OBJECT
public:
    explicit ImageHelper(QObject *parent = nullptr);

    static QPixmap pixmapFromIcon(QSize size, QIcon icon)
    {
        return icon.pixmap(size);
    }

    static QIcon iconFromSvgEmpty(QSize size = QSize(10,10)) {
        QPixmap pixmap(size);
        QPainter painter(&pixmap);
        const QRect bounding_rect(QPoint(0, 0), size);

        // 白色，实心模式(不透明)
        // 不使用画笔绘制一个区域
        painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
        painter.setPen(Qt::NoPen);
        painter.drawRect(bounding_rect);

        const int margin = 5;
        QRect target_rect = bounding_rect.adjusted(margin, margin, -margin, -margin);
        painter.setPen(Qt::SolidLine);
        painter.drawRect(target_rect);
        painter.drawLine(target_rect.topLeft(), target_rect.bottomRight() + QPoint(1, 1));
        painter.drawLine(target_rect.bottomLeft() + QPoint(0, 1), target_rect.topRight() + QPoint(1, 0));

        QIcon icon;
        icon.addPixmap(pixmap);
        return icon;
    }

    static QIcon iconFromSvgString(QSize &size, QByteArray svgs) {
        if (svgs.isEmpty()) { return iconFromSvgEmpty(size); }

        QPixmap pixmap(size);
        QPainter painter(&pixmap);
        const QRect bounding_rect(QPoint(0, 0), size);

        // 白色，实心模式(不透明)
        // 不使用画笔绘制一个区域
        painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
        painter.setPen(Qt::NoPen);
        painter.drawRect(bounding_rect);

        QSvgRenderer svg(svgs);
        QSize target_size = svg.defaultSize();
        target_size.scale(size, Qt::KeepAspectRatio);
        QRect target_rect = QRect(QPoint(0, 0), target_size);
        target_rect.translate(bounding_rect.center() - target_rect.center());
        svg.render(&painter, target_rect);

        QIcon icon;
        icon.addPixmap(pixmap);
        return icon;
    }

    static QPixmap pixmapFromSvgString(QSize size, QByteArray svgs) {
        return pixmapFromIcon(size, iconFromSvgString(size, svgs));
    }

    static QPixmap scaled(QSize size, QPixmap &pixmap) {
        return pixmap.scaled(size);
    }
    static QPixmap scaledKeepAspectRatio(QSize size, QPixmap &pixmap) {
        return pixmap.scaled(size, Qt::KeepAspectRatio);
    }
};

//QIcon PlantUMLPreview::iconFromSvgString(QSize size, const QByteArray &svgs)
//{
//    QPixmap pixmap(size);
//    QPainter painter(&pixmap);
//    const QRect bounding_rect(QPoint(0, 0), size);

//    if (!svgs.isEmpty()) {
//        painter.setRenderHint(QPainter::Antialiasing, true);

//        painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
//        painter.setPen(Qt::NoPen);
//        painter.drawRect(bounding_rect);

//        QSvgRenderer svg(svgs);
//        QSize target_size = svg.defaultSize();
//        target_size.scale(size, Qt::KeepAspectRatio);
//        QRect target_rect = QRect(QPoint(0, 0), target_size);
//        target_rect.translate(bounding_rect.center() - target_rect.center());
//        svg.render(&painter, target_rect);
//    } else {
//        painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
//        painter.setPen(Qt::NoPen);
//        painter.drawRect(bounding_rect);

//        const int margin = 5;
//        QRect target_rect = bounding_rect.adjusted(margin, margin, -margin, -margin);
//        painter.setPen(Qt::SolidLine);
//        painter.drawRect(target_rect);
//        painter.drawLine(target_rect.topLeft(), target_rect.bottomRight() + QPoint(1, 1));
//        painter.drawLine(target_rect.bottomLeft() + QPoint(0, 1), target_rect.topRight() + QPoint(1, 0));
//    }

//    QIcon icon;
//    icon.addPixmap(pixmap);
//    return icon;
//}

#endif // IMAGEHELPER_H
