#ifndef PLANTUMLPREVIEW_H
#define PLANTUMLPREVIEW_H

#include <QMainWindow>
#include <QProcess>
#include <qsciscintilla.h>

namespace Ui {
    class PlantUMLPreview;
}
class PlantUMLPreview : public QMainWindow
{
    Q_OBJECT
public:
    explicit PlantUMLPreview(QWidget *parent, QsciScintilla* pEdit);

signals:

public slots:
    void refreshUmlImage();
    void doRegeneraterPlantUmlImage();

private slots:
    void on_png_clicked();
    void on_svg_clicked();

private:
    bool checkJavaInstalled();
    void checkAndCopyPlantUmlJarFile();
    void setUiRadioButtonEnabled(bool enable);

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::PlantUMLPreview *ui;
    QsciScintilla* m_currentEdit;

private:
    QString s_qrcUmljar;
    QString s_fileUmlJar;

private:
    QString m_currentUmlGenType;
    QString m_currnetUmlContent;
    QByteArray m_currentUmlData;
    QPixmap m_currenUmlPixmap;

    // 标记检查已经安装 java SDK
    bool m_installedJavaSDK;
};

#endif // PLANTUMLPREVIEW_H
