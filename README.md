# Notepad-- PlantUML 预览插件

- 在线构建支持(暂不支持)
    ```cmake
    # 1. 查找 NotepadPlugin 模块
    # find_package(NotepadPlugin REQUIRED)

    # 2. 基于 git 仓库在线构建 plantuml 插件（online-plantuml-preview）
    # add_notepad_plugin_with_git(online-plantuml-preview
    #    https://gitee.com/zinface/notepad--plugin.plantuml-preview plantuml-with-java)

    # 3. 依赖 Qt5Svg
    # find_package(Qt5Svg)
    # target_link_libraries(online-plantuml Qt5::Svg)
    ```

- 效果图

    > 注意: 依赖于 java 环境，只有安装了 java 环境才能正常工作

    ![](assets/20230214002234.png)  